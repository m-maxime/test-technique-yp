"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Question_1 = __importDefault(require("./Question"));
class Entries {
    static async askGridProperties() {
        const answer = await (0, Question_1.default)('Dimensions de la grille : x y (exemple: 10 10) ? ');
        const [x, y] = answer.split(' ');
        const xNumber = parseInt(x);
        const yNumber = parseInt(y);
        if (isNaN(xNumber) || isNaN(yNumber)) {
            Entries.askGridProperties();
        }
        return [xNumber, yNumber];
    }
    static async askVacuumProperties() {
        const answer = await (0, Question_1.default)('Position initiales de l\'aspirateur : x y d (exemple: 5 5 N ) ? ');
        const [x, y, orientation] = answer.split(' ');
        const xNumber = parseInt(x);
        const yNumber = parseInt(y);
        if (isNaN(xNumber) || isNaN(yNumber) || ['N', 'E', 'W', 'S'].indexOf(orientation) === -1) {
            Entries.askVacuumProperties();
        }
        return [xNumber, yNumber, orientation];
    }
    static async askVacuumInstructions() {
        const answer = await (0, Question_1.default)('En attente d\'instructions (exemple: DADADADAA) : ');
        return answer;
    }
}
exports.default = Entries;
