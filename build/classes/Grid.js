"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Grid {
    rowNumber;
    colNumber;
    grid;
    constructor(rowNumber, colNumber) {
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.grid = [];
        this.initGrid();
    }
    getYMax() {
        return Math.max(this.rowNumber - 1, 0);
    }
    getXMax() {
        return Math.max(this.rowNumber - 1, 0);
    }
    getYMin() {
        return 1;
    }
    getXMin() {
        return 1;
    }
    getGrid() {
        return this.grid;
    }
    initGrid() {
        for (let y = 0; y < this.rowNumber; y++) {
            this.grid[y] = [];
            for (let x = 0; x < this.colNumber; x++) {
                this.grid[y][x] = 0;
            }
        }
    }
    placeVacuum(x, y, orientation) {
        if (typeof this.grid[y] === 'undefined' || typeof this.grid[y][x] === 'undefined') {
            return;
        }
        this.grid[y][x] = orientation;
    }
}
exports.default = Grid;
