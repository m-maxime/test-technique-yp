"use strict";
Object.defineProperty(exports, "__esModule", { value: true });
class Vacuum {
    y;
    x;
    orientation;
    grid;
    constructor(x, y, orientation, grid) {
        this.grid = grid;
        this.x = Math.max(Math.min(x, this.grid.getXMax()), this.grid.getYMin());
        this.y = Math.max(Math.min(y, this.grid.getYMax()), this.grid.getYMin());
        this.orientation = orientation;
        this.placeOnGrid();
    }
    getY() {
        return this.y;
    }
    getX() {
        return this.x;
    }
    getOrientation() {
        return this.orientation;
    }
    move() {
        if (this.orientation === 'N') {
            if (this.y + 1 < this.grid.getYMax()) {
                this.y += 1;
            }
        }
        else if (this.orientation === 'E') {
            if (this.x + 1 < this.grid.getXMax()) {
                this.x += 1;
            }
        }
        else if (this.orientation === 'S') {
            if (this.y - 1 > this.grid.getYMin()) {
                this.y -= 1;
            }
        }
        else if (this.orientation === 'O') {
            if (this.x - 1 > this.grid.getXMin()) {
                this.x -= 1;
            }
        }
    }
    rotate(orientationection) {
        if (orientationection === 'D') {
            if (this.orientation === 'N') {
                this.orientation = 'E';
            }
            else if (this.orientation === 'E') {
                this.orientation = 'S';
            }
            else if (this.orientation === 'S') {
                this.orientation = 'O';
            }
            else if (this.orientation === 'O') {
                this.orientation = 'N';
            }
        }
        else if (orientationection === 'G') {
            if (this.orientation === 'N') {
                this.orientation = 'O';
            }
            else if (this.orientation === 'O') {
                this.orientation = 'S';
            }
            else if (this.orientation === 'S') {
                this.orientation = 'E';
            }
            else if (this.orientation === 'E') {
                this.orientation = 'N';
            }
        }
    }
    displayLastPosition() {
        return `Position x=${this.getX()} y=${this.getY()} orientation=${this.getOrientation()}`;
    }
    run(instructions) {
        for (let charPos = 0; charPos < instructions.length; charPos++) {
            const char = instructions[charPos];
            if (char === 'A') {
                this.move();
            }
            else if (['D', 'G'].indexOf(char) !== -1) {
                this.rotate(char);
            }
            this.placeOnGrid();
        }
    }
    placeOnGrid() {
        this.grid.placeVacuum(this.getX(), this.getY(), this.getOrientation());
    }
}
exports.default = Vacuum;
