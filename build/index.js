"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const Vacuum_1 = __importDefault(require("./classes/Vacuum"));
const Entries_1 = __importDefault(require("./classes/Entries"));
const Grid_1 = __importDefault(require("./classes/Grid"));
async function start() {
    const [gridWidth, gridHeight] = await Entries_1.default.askGridProperties();
    const [vacuumX, vacuumY, vacuumOrientation] = await Entries_1.default.askVacuumProperties();
    const grid = new Grid_1.default(gridWidth, gridHeight);
    const vacuum = new Vacuum_1.default(vacuumX, vacuumY, vacuumOrientation, grid);
    while (1) {
        const instructions = await Entries_1.default.askVacuumInstructions();
        vacuum.run(instructions);
        console.info(vacuum.displayLastPosition());
    }
}
;
start();
