# Utilisation du projet 

## Utilisation de la version build

Aller dans le dossier build puis lancer la commande suivante :
```
node index.js
```

Il ne vous reste plus qu'à suivre les instructions. 

## Utilisation de la version de dev

Lancer les commande suivante :
```
npm install
```
Puis
```
npm run dev
```

## Note
Si l'aspirateur est positionné en dehors de la grille alors il sera automatiquement placé sur l'extrémité la plus proche.