import Vacuum from "./classes/Vacuum";
import Entries from "./classes/Entries";
import Grid from "./classes/Grid";

async function start() {
    const [gridWidth, gridHeight] = await Entries.askGridProperties();
    const [vacuumX, vacuumY, vacuumOrientation] = await Entries.askVacuumProperties();
    const grid = new Grid(gridWidth, gridHeight);
    const vacuum = new Vacuum(vacuumX, vacuumY, vacuumOrientation, grid);
    while(1) {
        const instructions = await Entries.askVacuumInstructions();
        vacuum.run(instructions); 
        console.info(vacuum.displayLastPosition());
    }
};

start();