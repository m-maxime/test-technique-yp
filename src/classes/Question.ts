import * as readline from 'node:readline/promises';
import { stdin as input, stdout as output } from 'node:process';

const question = (questionTxt: string) => {
    const rl = readline.createInterface({input, output});
    return new Promise<string>(resolve => rl.question(questionTxt).then((answer: string) => resolve(answer)))
        .finally(() => rl.close());
    }

export default question;