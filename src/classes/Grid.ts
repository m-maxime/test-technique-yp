import Vacuum from "./Vacuum";

export default class Grid {
    rowNumber: number;
    colNumber: number;
    grid: (string | number)[][];
    constructor(rowNumber: number, colNumber: number) {
        this.rowNumber = rowNumber;
        this.colNumber = colNumber;
        this.grid = [];
        this.initGrid();
    }

    getYMax(): number {
        return  Math.max(this.rowNumber - 1, 0);
    }
    getXMax(): number {
        return Math.max(this.rowNumber - 1, 0);
    }

    getYMin(): number {
        return 1;
    }

    getXMin(): number {
        return 1;
    }

    getGrid(): (string | number)[][] {
        return this.grid;
    }

    initGrid(): void {
        for (let y = 0; y < this.rowNumber; y++) {
            this.grid[y] = [];
            for (let x = 0; x < this.colNumber; x++) {
                this.grid[y][x] = 0;
            }
        }
    }

    placeVacuum(x: number, y: number, orientation: string): void {
        if (typeof this.grid[y] === 'undefined' || typeof this.grid[y][x] === 'undefined') {
            return;
        }
        this.grid[y][x] = orientation;
    }
}