
import question from './Question';

export default class Entries {
    static async askGridProperties(): Promise<[number, number]> {
        const answer = await question('Dimensions de la grille : x y (exemple: 10 10) ? ');
        const [x,y] = answer.split(' ');
        const xNumber = parseInt(x);
        const yNumber = parseInt(y);
        if (isNaN(xNumber) || isNaN(yNumber)) {
            Entries.askGridProperties()
        }

        return [xNumber , yNumber];
    }

    static async askVacuumProperties(): Promise<[number, number, string]> {
        const answer = await question('Position initiales de l\'aspirateur : x y d (exemple: 5 5 N ) ? ');
        const [x,y,orientation] = answer.split(' ');
        const xNumber = parseInt(x);
        const yNumber = parseInt(y);
        if (isNaN(xNumber) || isNaN(yNumber) || ['N','E','W','S'].indexOf(orientation) === -1) {
            Entries.askVacuumProperties()
        }

        return [xNumber , yNumber, orientation];
    }

    static async askVacuumInstructions(): Promise<string> {
        const answer = await question('En attente d\'instructions (exemple: DADADADAA) : ');        
        return answer;
    }
}